import express from "express";
import {
    categoryValidation,
    loginValidation,
    productValidation,
    registerValidation
} from "./src/validations/validations.js";
import mongoose from "mongoose";
import {getAboutMe, login, register} from "./src/controllers/UserController.js";
import cors from "cors";
import checkAuth from "./src/utils/checkAuth.js";
import {createCategory, getAllCategories} from "./src/controllers/CategoryController.js";
import checkAdmin from "./src/utils/checkAdmin.js";
import multer from "multer";
import {createUpload, getAllUploads} from "./src/controllers/MediaObjectController.js";
import {createProduct, getAllProducts} from "./src/controllers/ProductController.js";



const app = express();
app.use(express.json());
app.use('/uploads', express.static('uploads'));
app.use(cors());


mongoose
    .connect("mongodb+srv://dadajonovmax:717565boy@cluster0.b5lgurd.mongodb.net/dadajonovshop?retryWrites=true&w=majority")
    .then(() => {
        console.log("DB is ok")
    })
    .catch((err) =>{
        console.log("DB is error: ", err);
    })

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});
const upload = multer({storage});









//User start
app.post("/users/auth", registerValidation, login);
app.post('/users', loginValidation, register);
app.get('/users/about_me', checkAuth, getAboutMe);
//User end

//Upload start
app.post('/uploads', checkAuth, checkAdmin, upload.single('image'), createUpload);
app.get('/uploads', checkAuth, getAllUploads);
//Upload end

//Product start
app.post('/products', checkAuth, checkAdmin, productValidation, createProduct);
app.get('/products', getAllProducts);
//Product end


//Category start
app.post('/categories', checkAuth, checkAdmin, categoryValidation, createCategory);
app.get('/categories', getAllCategories);
//Category end


app.listen(3003, (err) => {
    if(err) {
        console.log("Server erorr");
    }
    console.log("Server ok")
});