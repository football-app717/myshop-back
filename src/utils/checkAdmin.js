import UserModel from "../models/User.js";


// Authorization middleware
export default  async (req, res, next) => {
    const user = await UserModel.findById(req.userId);

    console.log(user);

    if (user.roles.includes("ROLE_ADMIN")) {
        console.log(user.roles.includes("ROLE_ADMIN"))
        next();
    } else {
        res.status(403).json({ error: 'Access Denied' });
    }
};