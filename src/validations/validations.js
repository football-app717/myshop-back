import {body} from "express-validator";


export const loginValidation = [
    body("name", "Error in name")
        .isString()
        .withMessage("Name should be string")
        .notEmpty()
        .withMessage("Name can not be empty")
        .isLength({min: 3})
        .withMessage("Name is too short"),
    body("sname", "Error in second name")
        .isString()
        .withMessage("Second name should be string")
        .notEmpty()
        .withMessage("Second name can not be empty")
        .isLength({min: 3})
        .withMessage("Second name is too short"),
    body("email", "Error in email")
        .isEmail()
        .notEmpty()
        .withMessage("Email can not be empty"),
    body("password", "Error in password")
        .isString()
        .withMessage("Password should be string")
        .isLength({min: 5})
        .withMessage("Password is too short")
];


export const registerValidation = [
    body('email', 'Error in email')
        .isEmail()
        .notEmpty()
        .withMessage("Email can not be empty"),
    body("password", "Error in password")
        .isString()
        .withMessage("Password should be string")
        .isLength({min: 5})
        .withMessage("Password is too short")
];


export const categoryValidation = [
    body('name', 'Error in name')
        .isString()
        .withMessage("Name should be the string")
        .isLength({min: 3})
        .withMessage('Name is too short')
];


export const productValidation = [
    body('image', 'Error in image')
        .notEmpty()
        .withMessage('Image can not be empty')
        .isString()
        .withMessage("Name should be the string"),
    body('name', 'Error in name')
        .isString()
        .withMessage("Name should be the string")
        .isLength({min: 3})
        .withMessage('Name is too short'),
    body('description', 'Error in description')
        .notEmpty()
        .withMessage('Description can not be empty')
        .isString()
        .withMessage("Description should be the string"),
    body('price', 'Error in price')
        .notEmpty()
        .withMessage('Price can not be empty')
        .isString()
        .withMessage("Price should be the string"),
];

