import mongoose from "mongoose";


const CategoryShema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}, {timestamps: true});




export default mongoose.model('Category', CategoryShema);