import mongoose from "mongoose";


const ProductSchema = new mongoose.Schema({
    image: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "MediaObject",
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    }
}, {timestamps: true});


export default mongoose.model("Product", ProductSchema);

