import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    sname: {
        type: String,
        required: true
    },
    roles: {
      type: Array,
      default: ['ROLE_USER']
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    passwordHash: {
        type: String,
        required: true
    }
}, {timestamps: true});


export default mongoose.model('User', UserSchema);
