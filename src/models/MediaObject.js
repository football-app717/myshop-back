import mongoose from "mongoose";


const MediaObjectSchema = new mongoose.Schema({
    contentUrl: {
        type: String,
        required: true
    }
}, {timestamps: true});


export default mongoose.model('MediaObject', MediaObjectSchema);