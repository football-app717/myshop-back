import {validationResult} from "express-validator";
import CategoryModel from "../models/Category.js"
import category from "../models/Category.js";

export const createCategory = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json(errors.array())
        }


        const doc = new CategoryModel({
            name: req.body.name
        });

        const category = await doc.save();
        const {...categoryData} = category._doc;

        return res
            .status(201)
            .json({
                ...categoryData
            })


    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in CreateCategory"
            })
    }
};



export const getAllCategories = async (req, res) => {
    try {
        const categories = await CategoryModel.find();

        if(!categories) {
            return res
                .status(400)
                .json({
                    message: "Categories are empty"
                })
        }


        return res
            .status(200)
            .json(categories)
    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in getAllCategories"
            })
    }
}