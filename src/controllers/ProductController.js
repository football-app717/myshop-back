import {validationResult} from "express-validator";
import ProductModel from "../models/Product.js";


export const createProduct = async (req, res) => {
    try {
        const errors = validationResult(req);

        if(!errors.isEmpty()) {
            return res
                .status(400)
                .json({
                    message: "Products are empty"
                })
        }

        const doc = new ProductModel({
            image: req.body.image,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        });

        const product = await doc.save();

        const {...products} = product._doc;

        return res
            .status(200)
            .json({
                ...products
            })
    } catch (err) {
        return res
            .status(500)
            .json({
                message: 'Internal error in createProduct',
                error: err
            })
    }
};



export const getAllProducts = async (req, res) => {
    try {
        const products = await ProductModel.find()
            .populate({path: "image"})
            .exec();

        if(!products) {
            return res
                .status(400)
                .json({
                    message: "Products are empty"
                })
        }

        return res
            .status(200)
            .json(products);


    } catch (err) {
        return res
            .status(500)
            .json({
                message: 'Internal error in getAllProducts',
                error: err
            })
    }
}