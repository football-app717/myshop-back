import MediaObjectModel from "../models/MediaObject.js";


export const createUpload = async (req, res) => {
    try {
        if(!req.file) {
            return res
                .status(400)
                .json({
                    message: "File is not uploaded"
                })
        }

        const allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
        if (!allowedMimeTypes.includes(req.file.mimetype)) {
            return res.status(400).json({message: 'Only image files are allowed'});
        }

        const doc = new MediaObjectModel({
            contentUrl: `/${req.file.originalname}`
        });

        const mediaObject = await doc.save();

        const {...mediaObjects} = mediaObject._doc;

        return res
            .status(200)
            .json({
                ...mediaObjects
            })
    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in createUpload"
            })
    }
};


export const getAllUploads = async (req, res) => {
    try {
        const mediaObject = await MediaObjectModel.find();

        if(!mediaObject) {
            return res
                .status(400)
                .json({
                    message: "MediaObject is empty"
                })
        }


        return res
            .status(200)
            .json(mediaObject)
    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in getAllImages"
            })
    }
}